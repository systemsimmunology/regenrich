#' Constructing co-expression network
#' @description Constructing co-expression network by WGCNA.
#' @param expr gene expression data, it is either a matrix or a data frame. 
#' By default, each row represents a gene, each column represents a sample.
#' @param reg a vector of regulators. by default, these are transcription 
#' (co-)factors defined by three literatures/databases, namely RegNet, 
#' TRRUST, and Marbach2016.
#' @param rowSample logic, if TRUE, each row represents a sample. 
#' Otherwise, each column represents a sample. The default is FALSE.
#' @param softPower numeric, a soft power to achieve scale free topology. 
#' If not provided, the parameter will be picked automatically by 
#' \code{\link{plotSoftPower}} function from the WGCNA package.
#' @param networkType network type. Allowed values are (unique abbreviations of) 
#' "unsigned" (default), "signed", "signed hybrid". See \code{\link{adjacency}}.
#' @param TOMDenom a character string specifying the TOM variant to be used. 
#' Recognized values are "min" giving the standard TOM described in Zhang 
#' and Horvath (2005), and "mean" in which the min function in the 
#' denominator is replaced by mean. The "mean" may produce better results 
#' but at this time should be considered experimental.
#' @param RsquaredCut desired minimum scale free topology fitting index R^2.
#' The default is 0.85.
#' @param edgeThreshold numeric, the threshold to remove the low weighted 
#' edges, the default is NULL, which means no edges will be removed.
#' 
#' @return A list of two data frames. weightHi: weighted edges between 
#' regulators and targets; and allWeight: all weighted edges.
#' @import WGCNA
#' @export
#' @examples
#' \dontrun{
#' }
COEN = function(expr, reg = TFs$TF_name, rowSample = FALSE, softPower = NULL, 
                networkType = "unsigned", TOMDenom = "min", 
                RsquaredCut = 0.85, edgeThreshold = NULL) {
  # Enforce expression matrix to be the format that each row is a sample.
  if (!rowSample) {
    expr = t(expr)
    rowSample = !rowSample
  }
  
  # Find soft power if not provided
  if (is.null(softPower)) {
    powerVector = c(1:10, seq(12, 20, by = 2))
    sft = plotSoftPower(expr, powerVector = powerVector, 
                        RsquaredCut = RsquaredCut, networkType = networkType, 
                        verbose = 0)
    softPower = sft$powerEstimate
  }
  # stopifnot((!is.null(softPower)) & (!is.na(softPower)))
  if (is.null(softPower) || is.na(softPower)){
    stop("RsquaredCut is too high to achieve.")
  }
  
  # Adjacency matrix and Topological Overlap Matrix (TOM)
  adjacency = adjacency(expr, power = softPower, type = networkType)
  TOM = TOMsimilarity(adjMat = adjacency, TOMType = networkType, 
                      TOMDenom = TOMDenom)
  # Edges
  edge = TOM2edge(TOM, edgeThreshold = edgeThreshold, 
                  nodeNames = colnames(adjacency))
  nEdge = nrow(edge)
  if (nEdge == 0) 
    stop("No edges were remained!")
  
  # Only remain the edges connecting regulators
  weightHi = subset(edge, (from.gene %in% reg) | (to.gene %in% reg))
  return(list(weightHi = weightHi, allWeight = edge))
}
