#' Calculating edge weight for gene regulatory network.
#' @description \code{grNet}, based on GENIE3, is the core function to construct the GRN 
#' network and evaluate the accuracy of prediction for each target gene.
#' @param inputMatrix expression matrix of regulators (genes). Each row 
#' is a sample, each column is a gene. This matrix can be standardized by
#' \code{\link{inOutput}} function.
#' @param outputMatrix expression matrix of all genes. Each row is a sample, 
#' each column is a gene. This matrix can be standardized by
#' \code{\link{inOutput}} function.
#' @param K integer or character. The number of features in each tree, 
#' can be either a integer number, \code{sqrt}, or \code{all}. 
#' \code{sqrt} denotes sqrt(the number of \code{reg}), \code{all} 
#' means the number of \code{reg}. The default is \code{sqrt}.
#' @param nbTrees integer. The number of trees. The default is 1000.
#' @param importanceMeasure character. The importance type in 
#' \code{importance}. importanceMeasure can be \code{\%IncMSE}  or 
#' \code{IncNodePurity}, corresponding to type = 1 and 2 in 
#' \code{importance}. The default is \code{IncNodePurity} 
#' (decrease in node impurity), which is faster than \code{\%IncMSE} 
#' (decrease in accuracy).
#' @param trace logic. To show the progress (default) or not. If 
#' \code{fast = TRUE}, \code{trace = FALSE} will be set.
#' @param fast logic. If \code{TRUE} parallel computating will be performed. The 
#' default is \code{FALSE}.
#' @param cores integer. The number of cores will be used. If \code{fast = TRUE}, 
#' then this parameter will be used. The default is the number of CPU 
#' cores of the computer minus 1.
#' @param seed integer. Random seed. The default is 1234.
#' @param ... the rest parameters in \code{\link{randomForest}} function. 
#'
#' @return a list of edge weight and model performance for each target gene.
#' @seealso \code{\link{GRN}}
#' @import randomForest
#' @import data.table
#' @import doParallel
#' @importFrom parallel detectCores
#' @export
#### GRN construction (parallel or not), supplementary methods ####
grNet = function (inputMatrix, outputMatrix, K = "sqrt", nbTrees = 1000, 
                  importanceMeasure = "IncNodePurity", seed = 1234, trace = TRUE, 
                  fast = FALSE, cores = max(detectCores()-1, 1), ...) {
  ## All of the matrixs are sample * gene
  # inputMatrix is Gene Expression matrix of TFs
  # outputMatrix is Gene Expression matrix of All genes (or TFs)
  
  # Report when parameter importanceMeasure is not correctly spelled
  if (importanceMeasure != "IncNodePurity" && importanceMeasure != 
      "%IncMSE") {
    stop("Parameter importanceMeasure must be \"IncNodePurity\" or \"%IncMSE\"")
  }
  # Check if nodesize parameter is in the input arguments
  args = list(...)
  nInArgs = "nodesize" %in% names(args)
  outputGeneNames = colnames(outputMatrix)
  inputGeneNames = colnames(inputMatrix)
  .local = function(targetGeneIdx, inputGeneNames, outputGeneNames, 
                    inputMatrix, outputMatrix, K, nbTrees, importanceMeasure, 
                    seed, trace, nodesizeInArgs = nInArgs, ...) {
    if (!is.null(seed)) {
      set.seed(seed)
    }
    num.samples = nrow(outputMatrix)
    num.genes = ncol(outputMatrix)
    if (trace) {
      cat(paste("Computing gene ", targetGeneIdx, "/", 
                num.genes, "\n", sep = ""))
      flush.console()
    }
    targetGeneName = outputGeneNames[targetGeneIdx]
    theseInputGeneNames = setdiff(inputGeneNames, targetGeneName)
    x = inputMatrix[, theseInputGeneNames]
    numInputGenes <- length(theseInputGeneNames)
    y = outputMatrix[, targetGeneName]
    if (class(K) == "numeric") {
      mtry = K
    } else if (K == "sqrt") {
      mtry = round(sqrt(numInputGenes))
    } else if (K == "all") {
      mtry = numInputGenes
    } else {
      stop("Parameter K must be \"sqrt\", or \"all\", or an integer")
    }
    if (trace) {
      cat(paste("K = ", mtry, ", ", nbTrees, " trees\n\n", sep = ""))
      flush.console()
    }
    if (importanceMeasure == "%IncMSE") {
      y = y
      importance0 = TRUE
      type0 = 1
    } else {
      y = y/sd(y)
      importance0 = FALSE
      type0 = 2
    }
    if (nodesizeInArgs) {
      rf = randomForest(x, y, mtry = mtry, ntree = nbTrees, 
                        importance = importance0, ...)
    } else {
      rf = randomForest(x, y, mtry = mtry, ntree = nbTrees, 
                        importance = importance0, nodesize = 1, ...)
    }
    im = importance(rf, type = type0)
    weight = data.table(from.gene = rownames(im), 
                        to.gene = rep(targetGeneName, nrow(im)), 
                        imp = im/num.samples)
    y_hat = predict(rf)
    return(list(weight = weight, mse = sum((y_hat - y)^2)/length(y), 
                r = as.numeric(cor(y_hat, y)), 
                p = cor.test(y_hat, y, alternative = "greater")$p.value, 
                pVarExplaned = rf$rsq[length(rf$rsq)] * 100))
  }
  
  idx = setNames(1:ncol(outputMatrix), nm = outputGeneNames)
  tic = system.time({
    if (fast) {
      cat("parallel computing:\n")
      cat(cores, "core(s) will be used.\n")
      cl = makeCluster(cores)
      registerDoParallel(cl)
      on.exit(stopCluster(cl))
      res = foreach(targetGeneIdx = idx, .packages = c("randomForest", "data.table")) %dopar% {
        .local(targetGeneIdx, inputGeneNames, outputGeneNames, 
               inputMatrix, outputMatrix, K, nbTrees, importanceMeasure, 
               seed, trace = FALSE, ...)
      }
      names(res) = outputGeneNames
    } else {
      cat("non-parallel computing\n")
      res = lapply(idx, function(targetGeneIdx) {
      .local(targetGeneIdx, inputGeneNames, outputGeneNames, 
             inputMatrix, outputMatrix, K, nbTrees, importanceMeasure, 
             seed, trace, ...)
    })
    }
  })
  print(tic)
  # weight = do.call("rbind", lapply(res, "[[", "weight"))
  weight = rbindlist(lapply(res, "[[", "weight"))
  colnames(weight) = c("from.gene", "to.gene", "weight")
  performance = data.table(gene = outputGeneNames, 
                           mse = do.call("c", lapply(res, "[[", "mse")), 
                           r = do.call("c", lapply(res, "[[", "r")), 
                           p = do.call("c", lapply(res, "[[", "p")), 
                           pVarExplaned = do.call("c", lapply(res, "[[", "pVarExplaned")))
  return(list(weight = weight, performance = performance))
}
