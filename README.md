# RegEnrich

RegEnrich is an open source R package that generates data-driven gene regulatory networks and performs enrichment analysis to rank the key regulators. 
RegEnrich produces a refined regulator-regulator network consisting of key regulatorys involved in the biological processes of interest to the user. 

# Installation

RegEnrich package depends on randomForest, ggplot2, fgsea, DOSE, 
data.table, BiocParallel, doParallel, WGCNA, and foreach packages. So 
make sure these packages are installed before installing RegEnrich.

```
# Install fgsea, BiocParallel, DOSE and WGCNA from Bioconductor.
source("https://bioconductor.org/biocLite.R")
biocLite(c("fgsea", "BiocParallel", "DOSE", "WGCNA", "DESeq2", "limma"))
```

* The easiest way to install RegEnrich is using the `install_bitbucket` 
function in `devtools` package.

```
# Install devtools
install.packages("devtools")
library(devtools)

# Install RegEnrich
install_bitbucket("systemsimmunology/regenrich") 
```

* Alternatively, install the source code.

Download the source package from Bitbucket and then run:

```
# Suppose the pacakge is saved in "./myFolder/", and the names is "RegEnrich_v1.0.0.tar.gz"
install.packages("./myFolder/RegEnrich_v1.0.0.tar.gz", repos = NULL, type = "source")
```

# Get help
To get the full list of help pages, please run:

```
help(package = "RegEnrich")
```
We also provide a step by step vignette to show how to perform 
regulator enrichment analysis.

```
vignette("RegEnrich")
```

If you are using RegEnrich in you research, please cite:

Tao W., Radstake T.R.D.J., Pandit A.
RegEnrich: an R package for gene regulator enrichment analysis
*XXX*, **XX**:XXX (2018).


